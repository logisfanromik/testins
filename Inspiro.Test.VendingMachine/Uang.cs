﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspiro.Test.VendingMachine
{
    public class Uang
    {
        public decimal UangTersedia { get; private set; }

        public Uang()
        {
            UangTersedia = 0;
        }

        public bool TambahUang(string amount)
        {
            if (!decimal.TryParse(amount, out decimal amountInserted))
            {
                amountInserted = 0;
                return false;
            }
            this.UangTersedia += amountInserted;
            return true;
        }

        public bool DeleteUang(decimal amountremove)
        {
            if (this.UangTersedia <= 0)
            {
                return false;
            }

            this.UangTersedia -= amountremove;
            return true;
        }
    }
}

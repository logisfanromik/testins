﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspiro.Test.VendingMachine
{
    public class ChipsItem : BaseItem
    {
        public string success = "Silakan Ambil Chips Anda!";
        public ChipsItem(int id, string name, decimal price, int remain)
                : base(id,
                       name,
                       price,
                       remain
                       )
        {
        }
    }
}

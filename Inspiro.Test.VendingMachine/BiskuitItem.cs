﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspiro.Test.VendingMachine
{
    
    public class BiskuitItem : BaseItem
    {
        public string success = "Silakan Ambil Biskuit Anda!";
        public BiskuitItem(int id, string name, decimal price, int remain)
                : base(id,
                       name,
                       price,
                       remain
                       )
        {
        }
    }
}

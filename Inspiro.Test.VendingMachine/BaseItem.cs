﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspiro.Test.VendingMachine
{
    public class BaseItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int ItemRemaining {get; set; }

        public BaseItem(int id, string name, decimal price, int remain)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.ItemRemaining = remain;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspiro.Test.VendingMachine
{
    public interface IVendingMachine
    {
        public void ShowMenu();
    }


    public class VendingMachine : IVendingMachine
    {
        private readonly IVendingMachineService service;
        public Uang uang { get; }
        public VendingMachine(IVendingMachineService service)
        {
            this.service = service;
            uang = new Uang();
        }

        public void ShowMenu()
        {
            Console.WriteLine("Selamat Datang");
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Menu");
                Console.WriteLine("1. Lihat Product");
                Console.WriteLine("2. Beli");
                Console.WriteLine("Q. Keluar");

                Console.Write("Silakan Pilih Menu ? ");
                string input = Console.ReadLine();

                if (input == "1")
                {
                    Console.WriteLine("Displaying Items");
                    ShowItems();
                }
                else if (input == "2")
                {
                    ShowBeli();
                }
                else if (input.ToUpper() == "Q")
                {
                    Console.WriteLine("Quitting");
                    break;
                }
                else
                {
                    Console.WriteLine("Please try again");
                }

                Console.ReadLine();
                Console.Clear();
            }
        }

        private void ShowItems()
        {
            var items = service.GetAllItem();
            if (items != null)
            {
                Console.WriteLine($"\n\n{"#".PadRight(5)} {"Stok"} { "Produk".PadRight(47) } { "Harga".PadLeft(7)}");
                foreach (KeyValuePair<string, BaseItem> item in items)
                {
                    if (item.Value.ItemRemaining > 0)
                    {
                        string itemNumber = item.Value.Id.ToString().PadRight(5);
                        string itemsRemaining = item.Value.ItemRemaining.ToString().PadRight(5);
                        string productName = item.Value.Name.PadRight(40);
                        string price = item.Value.Price.ToString("C").PadLeft(5);
                        Console.WriteLine($"{itemNumber} {itemsRemaining} {productName} Harga: {price} ");
                    }
                    else
                    {
                        Console.WriteLine($"{item.Value.ToString()}: {item.Value.Name} Habis.");
                    }
                }
            }
            else
            {
                Console.WriteLine("item kosong");
            }
        }

        private void ShowBeli()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine("Silakan Pilih Menu!");
                Console.WriteLine("1. Masukkan Uang");
                Console.WriteLine("2. Pilih Produk");
                Console.WriteLine("3. Transaksi Selesai");
                Console.WriteLine("4. Kembali ke Awal");
                Console.WriteLine($"Uang Anda: Rp. {uang.UangTersedia.ToString("C")}");
                Console.Write("Silakan Pilih? ");
                string input = Console.ReadLine();

                if (input == "1")
                {
                    bool salah = true;
                    Console.WriteLine(">>> Masukkan Uang Anda?");
                    while (true)
                    {
                        if (salah)
                            Console.WriteLine(">>> Uang Yang Anda Masukkan Tidak Diterima, Silakan Masukkan Uang Anda Kembali?");

                        Console.Write("2000, 5000, 10000, 20000, 50000 (Rp.) ");
                        string duit = Console.ReadLine();
                        if (duit == "2000"
                            || duit == "5000"
                            || duit == "10000"
                            || duit == "20000"
                            || duit == "50000")
                        {
                            if (!uang.TambahUang(duit))
                            {
                                Console.WriteLine("duit yang anda masukan tidak valid, silakan masukkan kembali.");
                                salah = false;
                            }
                            else
                            {
                                ShowBeli();
                                break;
                            }
                        }
                        else
                        {
                            salah = true;
                        }
                    }

                }
                else if (input == "2")
                {
                    decimal murah = service.GetHargaPalingMurah();
                    bool beli = uang.UangTersedia > murah;
                    if (beli)
                    {
                        while (true)
                        {
                            ShowItems();
                            Console.Write(">>> Silakan Pilih Item Yang Ingin Anda Beli!");
                            string pilihitem = Console.ReadLine();
                            if (int.TryParse(pilihitem, out int id))
                            {
                                var item = service.GetItem(id);
                                if (item != null)
                                {
                                    if (item is BiskuitItem)
                                    {
                                        BiskuitItem biskuit = (BiskuitItem)item;
                                        if (biskuit.ItemRemaining <= 0)
                                        {
                                            Console.Write("Stok Kosong");
                                        }
                                        else if(biskuit.Price > uang.UangTersedia)
                                        {
                                            Console.Write("Uang Tidak Cukup");
                                            ShowBeli();
                                            break;
                                        }
                                        else
                                        {
                                            bool trans = uang.DeleteUang(biskuit.Price);
                                            if (trans)
                                                trans = service.UpdateRemainingBiskuitItem();

                                            if (trans)
                                                Console.Write(biskuit.success);
                                            
                                        }
                                        
                                    }
                                    else if (item is ChipsItem)
                                    {
                                        ChipsItem chipsItem = (ChipsItem)item;
                                        if (chipsItem.ItemRemaining <= 0)
                                        {
                                            Console.Write("Stok Kosong");
                                        }
                                        else if (chipsItem.Price > uang.UangTersedia)
                                        {
                                            Console.Write("Uang Tidak Cukup");
                                            ShowBeli();
                                            break;
                                        }
                                        else
                                        {
                                            bool trans = uang.DeleteUang(chipsItem.Price);
                                            if (trans)
                                                trans = service.UpdateRemainingChipsItem();

                                            if (trans)
                                                Console.Write(chipsItem.success);
                                        }
                                    }
                                    else if (item is CokelatItem)
                                    {
                                        CokelatItem cokelatItem = (CokelatItem)item;
                                        if (cokelatItem.ItemRemaining <= 0)
                                        {
                                            Console.Write("Stok Kosong");
                                        }
                                        else if (cokelatItem.Price > uang.UangTersedia)
                                        {
                                            Console.Write("Uang Tidak Cukup");
                                            ShowBeli();
                                            break;
                                        }
                                        else
                                        {
                                            bool trans = uang.DeleteUang(cokelatItem.Price);
                                            if (trans)
                                                trans = service.UpdateRemainingCokelatItem();

                                            if (trans)
                                                Console.Write(cokelatItem.success);
                                        }
                                    }
                                    else if (item is OreoItem)
                                    {
                                        OreoItem oreoItem = (OreoItem)item;
                                        if (oreoItem.ItemRemaining <= 0)
                                        {
                                            Console.Write("Stok Kosong");
                                        }
                                        else if (oreoItem.Price > uang.UangTersedia)
                                        {
                                            Console.Write("Uang Tidak Cukup");
                                            ShowBeli();
                                            break;
                                        }
                                        else
                                        {
                                            bool trans = uang.DeleteUang(oreoItem.Price);
                                            if (trans)
                                                trans = service.UpdateRemainingOreoItem();

                                            if (trans)
                                                Console.Write(oreoItem.success);
                                        }
                                    }
                                    else if (item is TangoItem)
                                    {
                                        TangoItem tangoItem = (TangoItem)item;
                                        if (tangoItem.ItemRemaining <= 0)
                                        {
                                            Console.Write("Stok Kosong");
                                        }
                                        else if (tangoItem.Price > uang.UangTersedia)
                                        {
                                            Console.Write("Uang Tidak Cukup");
                                            ShowBeli();
                                            break;
                                        }
                                        else
                                        {
                                            bool trans = uang.DeleteUang(tangoItem.Price);
                                            if (trans)
                                                trans = service.UpdateRemainingTangoItem();

                                            if (trans)
                                                Console.Write(tangoItem.success);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Console.Write("!Uang Anda Tidak Cukup!");
                    }

                }
                else if (input == "3")
                {
                    decimal uangkembali = uang.UangTersedia;
                    if (uangkembali > 0)
                    {
                        if (uang.DeleteUang(uangkembali))
                        {
                            Console.WriteLine($"Selesai, Silakan Ambil Sisa Uang Anda Rp. {uangkembali.ToString("C")}");
                        }
                    }
                    else
                    {
                        Console.WriteLine("selesai");
                        break;
                    }
                }
                else if (input.ToUpper() == "4")
                {
                    Console.WriteLine("Kembali");
                    break;
                }
                else
                {
                    Console.WriteLine("Please try again");
                }

                Console.ReadLine();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspiro.Test.VendingMachine
{
    public interface IVendingMachineService
    {
        Dictionary<string, BaseItem> GetAllItem();
        BaseItem GetItem(int Id);
        decimal GetHargaPalingMurah();
        bool UpdateRemainingBiskuitItem();
        bool UpdateRemainingChipsItem();
        bool UpdateRemainingCokelatItem();
        bool UpdateRemainingOreoItem();
        bool UpdateRemainingTangoItem();
    }
    public class VendingMachineService : IVendingMachineService
    {
        private readonly IJsonFileReader jsonFileReader;
        public VendingMachineService(IJsonFileReader jsonFileReader)
        {
            this.jsonFileReader = jsonFileReader;
        }
        public Dictionary<string, BaseItem> GetAllItem()
        {
            try
            {
                return jsonFileReader.GetItems();
            }
            catch(Exception x)
            {
                throw x;
            }
        }

        public BaseItem GetItem(int id)
        {
            try
            {
                var data = jsonFileReader.GetData();
                if(data.Biskuit != null && data.Biskuit.Id == id)
                {
                    return data.Biskuit;
                }
                else if (data.Chips != null && data.Chips.Id == id)
                {
                    return data.Chips;
                }
                else if (data.Cokelat != null && data.Cokelat.Id == id)
                {
                    return data.Cokelat;
                }
                else if (data.Oreo != null && data.Oreo.Id == id)
                {
                    return data.Oreo;
                }
                else if (data.Tango != null && data.Tango.Id == id)
                {
                    return data.Tango;
                }
                return null;
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        public decimal GetHargaPalingMurah()
        {
            try
            {
                decimal result = 0;
                var data = jsonFileReader.GetData();
                if (data.Biskuit != null && data.Biskuit.Price > result)
                {
                    result = data.Biskuit.Price;
                }
                else if (data.Chips != null && data.Chips.Price > result)
                {
                    result = data.Chips.Price;
                }
                else if (data.Cokelat != null && data.Cokelat.Price > result)
                {
                    result = data.Cokelat.Price;
                }
                else if (data.Oreo != null && data.Oreo.Price > result)
                {
                    result = data.Oreo.Price;
                }
                else if (data.Tango != null && data.Tango.Price > result)
                {
                    result = data.Tango.Price;
                }
                return result;
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        public bool UpdateRemainingBiskuitItem()
        {
            try
            {
                if(jsonFileReader.UpdateRemaining("Biskuit", 1))
                {
                    return true;
                }
                return false;
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        public bool UpdateRemainingChipsItem()
        {
            try
            {
                if (jsonFileReader.UpdateRemaining("Chips", 1))
                {
                    return true;
                }
                return false;
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        public bool UpdateRemainingCokelatItem()
        {
            try
            {
                if (jsonFileReader.UpdateRemaining("Cokelat", 1))
                {
                    return true;
                }
                return false;
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        public bool UpdateRemainingOreoItem()
        {
            try
            {
                if (jsonFileReader.UpdateRemaining("Oreo", 1))
                { 
                    return true;
                }
                return false;
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        public bool UpdateRemainingTangoItem()
        {
            try
            {
                if (jsonFileReader.UpdateRemaining("Tango", 1))
                {
                    return true;
                }
                return false;
            }
            catch (Exception x)
            {
                throw x;
            };
        }
    }
}

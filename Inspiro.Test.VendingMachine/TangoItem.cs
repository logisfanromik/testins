﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspiro.Test.VendingMachine
{
    public class TangoItem : BaseItem
    {
        public string success = "Silakan Ambil Tanggo Anda!";
        public TangoItem(int id, string name, decimal price, int remain)
                : base(id,
                       name,
                       price,
                       remain
                       )
        {
        }
    }
}

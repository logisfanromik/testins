﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspiro.Test.VendingMachine
{
    public class OreoItem : BaseItem
    {
        public string success = "Silakan Ambil Oreo Anda!";
        public OreoItem(int id, string name, decimal price, int remain)
                : base(id,
                       name,
                       price,
                       remain
                       )
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspiro.Test.VendingMachine
{
    public class CokelatItem : BaseItem
    {
        public string success = "Silakan Ambil Cokelat Anda!";
        public CokelatItem(int id, string name, decimal price, int remain)
                : base(id,
                       name,
                       price,
                       remain
                       )
        {
        }
    }
}

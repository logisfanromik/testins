﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Inspiro.Test.VendingMachine
{
    public interface IJsonFileReader
    {
        public Dictionary<string, BaseItem> GetItems();
        public JsonFileItem GetData();
        public bool UpdateRemaining(string item, int qty);
    }
    public class JsonFileReader : IJsonFileReader
    {
        private string filename = "data.json";
        public Dictionary<string, BaseItem> GetItems()
        {
            try
            {
                Dictionary<string, BaseItem> items = new Dictionary<string, BaseItem>();
                var data = GetData();
                if (data.Biskuit != null)
                    items.Add("Biskuit", data.Biskuit);
                if (data.Biskuit != null)
                    items.Add("Chips", data.Chips);
                if (data.Biskuit != null)
                    items.Add("Cokelat", data.Cokelat);
                if (data.Biskuit != null)
                    items.Add("Oreo", data.Oreo);
                if (data.Biskuit != null)
                    items.Add("Tango", data.Tango);
                return items;
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        public JsonFileItem GetData()
        {
            try
            {
                JsonFileItem data = null;
                if (File.Exists(filename))
                {
                    var jsonString = File.ReadAllText(filename);
                    data = JsonConvert.DeserializeObject<JsonFileItem>(jsonString);
                }
                return data;
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        public bool UpdateRemaining(string item, int qty)
        {

            try
            {
                if (File.Exists(filename))
                {
                    var jsonString = File.ReadAllText(filename);
                    var jObject = JObject.Parse(jsonString);
                    int remaining = jObject[item]["ItemRemaining"].ToObject<int>();
                    jObject[item]["ItemRemaining"] = remaining - qty;
                    string output = JsonConvert.SerializeObject(jObject, Formatting.Indented);
                    File.WriteAllText(filename, output);
                    return true;

                }
                return false;
            }
            catch (Exception x)
            {
                throw x;
            }
        }
    }


    public class JsonFileItem
    {
        public BiskuitItem Biskuit { get; set; }
        public ChipsItem Chips { get; set; }
        public CokelatItem Cokelat { get; set; }
        public OreoItem Oreo { get; set; }
        public TangoItem Tango { get; set; }
    }
}

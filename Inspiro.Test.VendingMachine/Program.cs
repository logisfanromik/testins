﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Inspiro.Test.VendingMachine
{
    public class Program
    {
        private readonly IVendingMachine vm;

        public Program(IVendingMachine vm)
        {
            this.vm = vm;
        }

        public void Run()
        {
            vm.ShowMenu();
        }

        public static void Main(string[] args)
        {
            IServiceProvider serviceProvider = RegisterServices();

            Program program = serviceProvider.GetService<Program>();

            program.Run();

            DisposeServices(serviceProvider);
        }
        private static IServiceProvider RegisterServices()
        {
            var services = new ServiceCollection();

            services.AddScoped<IJsonFileReader, JsonFileReader>();
            services.AddScoped<IVendingMachineService, VendingMachineService>();
            services.AddScoped<IVendingMachine, VendingMachine>();
            
            services.AddScoped<Program>(); //<-- NOTE THIS

            return services.BuildServiceProvider();
        }

        private static void DisposeServices(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                return;
            }
            if (serviceProvider is IDisposable sp)
            {
                sp.Dispose();
            }
        }

    }
}
